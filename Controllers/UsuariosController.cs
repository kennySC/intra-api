﻿using intraApi.Models;
using intraApi.Models.DTOS;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace intraApi.Controllers
{
    public class UsuariosController : ApiController
    {

        private string CMD_LOGIN = "Select * from usuarios u where u.email = @email and u.password = @pass";
        private string CMD_REGISTER = "Insert into usuarios (email, nombreCompleto, password, telefono, isActivo) values (@email, @nombre, @password, @telefono, @isActivo)";


        [HttpGet]
        [Route("api/Usuarios/login/{email}/{password}")]        
        public UsuarioLoginResponse login(string email, string password)
        {

            UsuarioLoginResponse response = new UsuarioLoginResponse(new Status() { code = -1, message = "Usuario no encontrado" }, null);
            Usuario usuario = null;

            string mycon = ConfigurationManager.AppSettings["dbconn"].ToString();
            MySqlConnection con = new MySqlConnection(mycon);                                   

            try
            {
                MySqlCommand cmd = new MySqlCommand(CMD_LOGIN, con);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@pass", password);

                con.Open();
                var reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    usuario = new Usuario( reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetBoolean(5));
                }

                con.Close();

                if (usuario != null)
                {
                    response.Data = usuario;
                    response.status.code = 0;
                    response.status.message = "exito";
                }

            }
            catch (Exception ex)
            {
                response.status.code = -2;
                response.status.message = $"Error - {ex.Message}";
                con.Close();
            }

            return response;
        }


        [HttpPost]
        [Route("api/Usuarios/register")]
        public Status registerUsuario(Usuario usuario)
        {
            Status response = new Status() { code = -1, message = "Error al insertar" };

            string mycon = ConfigurationManager.AppSettings["dbconn"].ToString();
            MySqlConnection con = new MySqlConnection(mycon);

            try
            {

                MySqlCommand cmd = new MySqlCommand(CMD_REGISTER, con);
                cmd.Parameters.AddWithValue("@email", usuario.email);
                cmd.Parameters.AddWithValue("@nombre", usuario.nombre);
                cmd.Parameters.AddWithValue("@password", usuario.password);
                cmd.Parameters.AddWithValue("@telefono", usuario.telefono);
                cmd.Parameters.AddWithValue("@isActivo", 1);

                con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

                response.code = 0;
                response.message = "success";

            }
            catch (Exception ex)
            {
                response.code = -2;
                response.message = $"Error -- {ex.Message}";
            }

            return response;
        }

        #region test

        //public string getAllUsers()
        //{

        //    string usuarios = "";

        //    List<Usuario> lsUsuarios = new List<Usuario>();

        //    string mycon = "server =localhost; Uid=root; port=3306; password = ; persistsecurityinfo = True; database =intra_db; SslMode = none";
        //    MySqlConnection con = new MySqlConnection(mycon);
        //    MySqlCommand cmd = null;
        //    try
        //    {
        //        cmd = new MySqlCommand("Select * from usuarios", con);
        //        con.Open();
        //        var reader = cmd.ExecuteReader();

        //        if (reader.HasRows)
        //        {
        //            while (reader.Read())
        //            {
        //                lsUsuarios.Add(new Usuario(
        //                            reader.GetInt32(0),
        //                            reader.GetString(1),
        //                            reader.GetString(2),
        //                            reader.GetString(3),
        //                            reader.GetString(4),
        //                            reader.GetBoolean(5)
        //                        ));
        //            }
        //        }

        //        con.Close();

        //        lsUsuarios.ForEach(u => usuarios += u.ToString());

        //    }
        //    catch (Exception ex)
        //    {
        //        con.Close();

        //        usuarios = "ERROR";
        //    }

        //    return usuarios;
        //}

        #endregion


    }
}
