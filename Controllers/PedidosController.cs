﻿using intraApi.Models;
using intraApi.Models.DTOS;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace intraApi.Controllers
{
    public class PedidosController : ApiController
    {
        private string CMD_GETPEDIDOS = "select p.id as 'idPedido', c.idCliente, c.idTienda, c.idMetodoPago, c.total, c.codigo,c.fechaPedido, c.isActivo " +
                                        "from pedidos p " +
                                        "where p.idCliente = @idCliente";

        private string CMD_GETPRODUCTOS = "select p.*, c.nombre as 'nomCate' " +
                                        "from productos p " +
                                        "inner join productos_pedido pp on p.id = pp.idProduto " +
                                        "inner join categoria c on p.idCategoria = c.id " +
                                        "where pp.idPedido = @idPedido";

        private string CMD_INSERTARPEDIDO= "insert into pedidos (idCliente,idTienda,idMetodoPago,total,codigo,fechaPedido,isActivo) values (@idCli, @idTie,@idPago,@total,@cod,@fechaPed,1)";

        private string CMD_INSERTARPEDIDOPRODUCTO = "insert into produtos_pedido (idPedido,idProduto,cantdad) values (@idPed, @idProd,@cant)";

        [HttpGet]
        [Route("api/Pedido/getPedido/{idCliente}")]
        public PedidoGetResponse getCarrito(string idCliente)
        {
            PedidoGetResponse response = new PedidoGetResponse(new Status() { code = 1, message = "Error" }, null);

            List<Pedido> pedidos = new List<Pedido>();

            string mycon = ConfigurationManager.AppSettings["dbconn"].ToString();
            MySqlConnection con = new MySqlConnection(mycon);

            try
            {
                MySqlCommand cmd = new MySqlCommand(CMD_GETPEDIDOS, con);
                cmd.Parameters.AddWithValue("@idCliente", idCliente);
                con.Open();
                var reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        pedidos.Add(new Pedido(reader.GetInt32("id"), reader.GetInt32("idCliente"), reader.GetInt32(" idTienda"),reader.GetInt32("idMetodoPago"),reader.GetDouble("total"),reader.GetString("codigo"),reader.GetDateTime("fechaPedido"), reader.GetInt32("isActivo")));
                    }
                }

                if (pedidos != null && pedidos.Count > 0)
                {
                    response.Data = pedidos;
                    response.status.code = 0;
                    response.status.message = "success";
                }
            }
            catch (Exception ex)
            {
                response.status.code = 2;
                response.status.message = $"CATCH - {ex.Message}";
            }

            return response;
        }

        [HttpPost]
        [Route("api/Pedido/getPedidoProductos")]
        public PedidoProductosGetResponse getCarritoProductos(Pedido pedido)
        {
            PedidoProductosGetResponse response = new PedidoProductosGetResponse(new Status() { code = 1, message = "Error" }, null);

            pedido.productosPedido = new List<Producto>();

            string mycon = ConfigurationManager.AppSettings["dbconn"].ToString();
            MySqlConnection con = new MySqlConnection(mycon);

            try
            {
                MySqlCommand cmd = new MySqlCommand(CMD_GETPRODUCTOS, con);
                cmd.Parameters.AddWithValue("@idCarrito", pedido.id);
                con.Open();
                var reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        pedido.productosPedido.Add(new Producto()
                        {
                            id = reader.GetInt32("id"),
                            categoria = new Categoria(reader.GetInt32("idCategoria"), reader.GetString("nomCat")),
                            cantidad = reader.GetInt32("cantidad"),
                            descuento = reader.GetDouble("descuento"),
                            idTienda = reader.GetInt32("idTienda"),
                            inStock = reader.GetBoolean("inStock"),
                            isActivo = reader.GetBoolean("isActivo"),
                            nombre = reader.GetString("nombre"),
                            precio = reader.GetDouble("precio")
                        });
                    }
                }

                if (pedido.productosPedido != null && pedido.productosPedido.Count > 0)
                {
                    response.Data = pedido;
                    response.status.code = 0;
                    response.status.message = "success";
                }
            }
            catch (Exception ex)
            {
                response.status.code = 2;
                response.status.message = $"CATCH - {ex.Message}";
            }

            return response;
        }

        [HttpPost]
        [Route("api/Pedido/insertarPedido")]
        public Status insertarPedido(Pedido pedido)
        {
            Status response = new Status() { code = -1, message = "Error al insertar" };

            string mycon = ConfigurationManager.AppSettings["dbconn"].ToString();
            MySqlConnection con = new MySqlConnection(mycon);

            try
            {
                
                MySqlCommand cmd = new MySqlCommand(CMD_INSERTARPEDIDO, con);
                cmd.Parameters.AddWithValue("@idCli", pedido.idCliente);
                cmd.Parameters.AddWithValue("@idTie", pedido.idTienda);
                cmd.Parameters.AddWithValue("@idPago", pedido.idMetodoPago);
                cmd.Parameters.AddWithValue("@total", pedido.total);
                cmd.Parameters.AddWithValue("@cod", pedido.codigo);
                cmd.Parameters.AddWithValue("@fechaPed", pedido.fechaPedido);

                con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

                response.code = 0;
                response.message = "success";

            }
            catch (Exception ex)
            {
                response.code = -2;
                response.message = $"Error -- {ex.Message}";
            }

            return response;
        }

        [HttpPost]
        [Route("api/Pedido/insertarPedidoProducto")]
        public Status insertarPedidoProducto(Productos_Pedido pedido_producto)
        {
            Status response = new Status() { code = -1, message = "Error al insertar" };

            string mycon = ConfigurationManager.AppSettings["dbconn"].ToString();
            MySqlConnection con = new MySqlConnection(mycon);

            try
            {
                
                MySqlCommand cmd = new MySqlCommand(CMD_INSERTARPEDIDOPRODUCTO, con);
                cmd.Parameters.AddWithValue("@idPed", pedido_producto.idPedido);
                cmd.Parameters.AddWithValue("@idProd", pedido_producto.idProducto);
                cmd.Parameters.AddWithValue("@cant", pedido_producto.cantidad);

                con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

                response.code = 0;
                response.message = "success";

            }
            catch (Exception ex)
            {
                response.code = -2;
                response.message = $"Error -- {ex.Message}";
            }

            return response;
        }
    }
}
