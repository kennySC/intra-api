﻿using intraApi.Models;
using intraApi.Models.DTOS;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace intraApi.Controllers
{    
    public class CategoriaController : ApiController
    {
        private string CMD_GETCATEGORIAS = "select * from categorias";
        private string CMD_INSERTAR = "insert into categorias (nombre) values (@nom)";
        private string CMD_ACTUALIZAR = "update categorias set nombre = @nom where id = @idCategoria";


        [HttpGet]
        [Route("api/Categorias/getCategorias")]
        public CategoriasGetReponse getCategorias ()
        {
            CategoriasGetReponse response = new CategoriasGetReponse(new Status() { code = 1, message = "Error" }, null);

            List<Categoria> categorias = new List<Categoria>();

            string mycon = ConfigurationManager.AppSettings["dbconn"].ToString();
            MySqlConnection con = new MySqlConnection(mycon);

            try
            {
                MySqlCommand cmd = new MySqlCommand(CMD_GETCATEGORIAS, con);

                con.Open();
                var reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        categorias.Add(new Categoria(reader.GetInt32("id"), reader.GetString("nombre")));
                    }
                }

                if (categorias != null && categorias.Count > 0)
                {
                    response.Data = categorias;
                    response.status.code = 0;
                    response.status.message = "success";
                }
            }
            catch (Exception ex)
            {
                response.status.code = 2;
                response.status.message = $"CATCH - {ex.Message}";
            }

            return response;
        }


        [HttpPost]
        [Route("api/Categorias/insertar")]
        public Status insertarCategoria(Categoria categoria)
        {
            Status response = new Status() { code = -1, message = "Error al insertar" };

            string mycon = ConfigurationManager.AppSettings["dbconn"].ToString();
            MySqlConnection con = new MySqlConnection(mycon);

            try
            {

                MySqlCommand cmd = new MySqlCommand(CMD_INSERTAR, con);
                cmd.Parameters.AddWithValue("@nom", categoria.nombre);

                con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

                response.code = 0;
                response.message = "success";

            }
            catch (Exception ex)
            {
                response.code = -2;
                response.message = $"Error -- {ex.Message}";
            }

            return response;
        }

        [HttpPost]
        [Route("api/Categorias/actualizar")]
        public Status actualizarCategoria(Categoria categoria)
        {
            Status response = new Status() { code = -1, message = "Error al actualizar" };

            string mycon = ConfigurationManager.AppSettings["dbconn"].ToString();
            MySqlConnection con = new MySqlConnection(mycon);

            try
            {

                MySqlCommand cmd = new MySqlCommand(CMD_ACTUALIZAR, con);
                cmd.Parameters.AddWithValue("@nom", categoria.nombre);
                cmd.Parameters.AddWithValue("@idCategoria", categoria.id);

                con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

                response.code = 0;
                response.message = "success";

            }
            catch (Exception ex)
            {
                response.code = -2;
                response.message = $"Error -- {ex.Message}";
            }

            return response;
        }

    }
}
