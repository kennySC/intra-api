﻿using intraApi.Models;
using intraApi.Models.DTOS;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace intraApi.Controllers
{
    public class CarritosController : ApiController
    {
        private string CMD_GETCARRITOS = "select c.id as 'idCarrito', c.idCliente, c.idTienda, c.isActivo "+
                                        "from carritos c "+
                                        "where c.idCliente = @idCliente";

        private string CMD_GETPRODUCTOS = "select p.*, c.nombre as 'nomCate' " +
                                        "from productos p " +
                                        "inner join productos_carrito pc on p.id = pc.idProducto "+
                                        "inner join categoria c on p.idCategoria = c.id " +
                                        "where pc.idCarrito = @idCarrito";

        private string CMD_INSERTARCARRITO = "insert into carritos (idCliente,idTienda,isActivo) values (@idCli, @idTie,1)";

        private string CMD_INSERTARCARRITOPRODUCTO = "insert into productos_carrito (idCarrito,idProduto,cantdad) values (@idCar, @idProd,@cant)";

        private string CMD_ACTUALIZAR = "update carritos set idCliente = @idCli, idTienda = @idTie where id = @idCarrito";

        [HttpGet]
        [Route("api/Carrito/getCarrito/{idCliente}")]
        public CarritoGetResponse getCarrito (string idCliente)
        {
            CarritoGetResponse response = new CarritoGetResponse(new Status() { code = 1, message = "Error" }, null);

            List<Carrito> carrito = new List<Carrito>(); 

            string mycon = ConfigurationManager.AppSettings["dbconn"].ToString();
            MySqlConnection con = new MySqlConnection(mycon);

            try
            {
                MySqlCommand cmd = new MySqlCommand(CMD_GETCARRITOS, con);
                cmd.Parameters.AddWithValue("@idCliente", idCliente);
                con.Open();
                var reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        carrito.Add(new Carrito(reader.GetInt32("id"), reader.GetInt32("idCliente"), reader.GetInt32(" idTienda"), reader.GetInt32("isActivo")));
                    }
                }

                if (carrito != null && carrito.Count > 0)
                {
                    response.Data = carrito;
                    response.status.code = 0;
                    response.status.message = "success";
                }
            }
            catch (Exception ex)
            {
                response.status.code = 2;
                response.status.message = $"CATCH - {ex.Message}";
            }

            return response;
        }

        [HttpPost]
        [Route("api/Carrito/getCarritoProductos")]
        public CarritoProductosGetResponse getCarritoProductos(Carrito carrito)
        {
            CarritoProductosGetResponse response = new CarritoProductosGetResponse(new Status() { code = 1, message = "Error" }, null);

            carrito.productosCarrito = new List<Producto>();

            string mycon = ConfigurationManager.AppSettings["dbconn"].ToString();
            MySqlConnection con = new MySqlConnection(mycon);

            try
            {
                MySqlCommand cmd = new MySqlCommand(CMD_GETPRODUCTOS, con);
                cmd.Parameters.AddWithValue("@idCarrito", carrito.id);
                con.Open();
                var reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        carrito.productosCarrito.Add(new Producto() {id =reader.GetInt32("id"),categoria = new Categoria(reader.GetInt32("idCategoria"),reader.GetString("nomCat")),cantidad = reader.GetInt32("cantidad"),
                        descuento = reader.GetDouble("descuento"), idTienda = reader.GetInt32("idTienda"),inStock = reader.GetBoolean("inStock"),isActivo = reader.GetBoolean("isActivo"), nombre = reader.GetString("nombre"),
                        precio=reader.GetDouble("precio")});
                    }
                }

                if (carrito.productosCarrito != null && carrito.productosCarrito.Count > 0)
                {
                    response.Data = carrito;
                    response.status.code = 0;
                    response.status.message = "success";
                }
            }
            catch (Exception ex)
            {
                response.status.code = 2;
                response.status.message = $"CATCH - {ex.Message}";
            }

            return response;
        }

        [HttpPost]
        [Route("api/Carrito/insertarCarrito")]
        public Status insertarCarrito(Carrito carrito)
        {
            Status response = new Status() { code = -1, message = "Error al insertar" };

            string mycon = ConfigurationManager.AppSettings["dbconn"].ToString();
            MySqlConnection con = new MySqlConnection(mycon);

            try
            {

                MySqlCommand cmd = new MySqlCommand(CMD_INSERTARCARRITO, con);
                cmd.Parameters.AddWithValue("@idCli", carrito.idCliente);
                cmd.Parameters.AddWithValue("@idTie", carrito.idTienda);

                con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

                response.code = 0;
                response.message = "success";

            }
            catch (Exception ex)
            {
                response.code = -2;
                response.message = $"Error -- {ex.Message}";
            }

            return response;
        }

        [HttpPost]
        [Route("api/Carrito/insertarCarritoProducto")]
        public Status insertarCarritoProducto(Productos_Carrito carrito)
        {
            Status response = new Status() { code = -1, message = "Error al insertar" };

            string mycon = ConfigurationManager.AppSettings["dbconn"].ToString();
            MySqlConnection con = new MySqlConnection(mycon);

            try
            {

                MySqlCommand cmd = new MySqlCommand(CMD_INSERTARCARRITOPRODUCTO, con);
                cmd.Parameters.AddWithValue("@idCar", carrito.idCarrito);
                cmd.Parameters.AddWithValue("@idProd", carrito.idProducto);
                cmd.Parameters.AddWithValue("@cant", carrito.cantidad);

                con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

                response.code = 0;
                response.message = "success";

            }
            catch (Exception ex)
            {
                response.code = -2;
                response.message = $"Error -- {ex.Message}";
            }

            return response;
        }
    }
}
