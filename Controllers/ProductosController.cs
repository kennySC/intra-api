﻿using intraApi.Models;
using intraApi.Models.DTOS;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace intraApi.Controllers
{
    public class ProductosController : ApiController
    {

        // Buscar productos por id de la tienda tienda
        private string CMD_PRODUCTOS_TIENDA = "select p.*, c.nombre as 'nomCat' from productos p " +
                                                "inner join tiendas t on p.idTienda = t.id " +
                                                "inner join categorias c on p.idCategoria = c.id " +
                                                "where t.id = @idTienda and p.isActivo = 1 and t.isActivo = 1";

        // Buscar productos por id de la categoria
        private string CMD_PRODUCTOS_CATEGORIA = "select p.*, c.nombre as 'nomCat' from productos p " +
                                                    "inner join tiendas t on p.idTienda = t.id " +
                                                    "inner join categorias c on p.idCategoria = c.id " +
                                                    "where c.id = @idCateg and p.isActivo = 1 and t.isActivo = 1";

        // Buscar productos por nombre del producto o de la categoria
        private string CMD_BUSCAR_PRODUCTOS_NOMBRE_P_T_C = "select p.*, c.nombre as 'nomCat' from productos p " +
                                                        "inner join tiendas t on p.idTienda = t.id " +
                                                        "inner join categorias c on p.idCategoria = c.id " +
                                                        "where (c.nombre like '%@nom%' or p.nombre like '%@nom%') and p.isActivo = 1 and t.isActivo = 1";

        // Insertar un nuevo producto
        private string CMD_INSERTAR = "insert into productos (idTienda, idCategoria, nombre, precio, descuento, inStock, cantidad, isActivo) " +
                                        "values(@idTienda, @idCat, @nom, @precio, @descuento, @inStock, @cantidad, @isActivo)";

        // Actualizar un producto ya existente
        private string CMD_ACTUALIZAR = "update productos set idCategoria = @idCat, nombre = @nom, precio = @precio, descuento = @descuento, inStock = @inStock, cantidad = @cantidad, isActivo = @isActivo " +
                                            "where id = @idProducto";


        [HttpGet]
        [Route("api/Productos/getProductosTienda/{idTienda}")]
        public ProductosGetResponse getProductosTienda(string idTienda)
        {
            ProductosGetResponse response = new ProductosGetResponse(new Status() { code = -1, message = "Productos no encontrados" }, null);

            if (idTienda != null && idTienda != "")
            {                

                List<Producto> productos = new List<Producto>();

                string mycon = ConfigurationManager.AppSettings["dbconn"].ToString();
                MySqlConnection con = new MySqlConnection(mycon);


                try
                {
                    MySqlCommand cmd = new MySqlCommand(CMD_PRODUCTOS_TIENDA, con);
                    cmd.Parameters.AddWithValue("@idTienda", idTienda);

                    con.Open();
                    var reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            productos.Add(
                                new Producto()
                                {
                                    id = reader.GetInt32("id"),
                                    idTienda = reader.GetInt32("idTienda"),
                                    nombre = reader.GetString("nombre"),
                                    precio = reader.GetDouble("precio"),
                                    descuento = reader.GetDouble("descuento"),
                                    inStock = (reader.GetInt32("inStock") == 1 ? true : false),
                                    cantidad = reader.GetInt32("cantidad"),
                                    isActivo = (reader.GetInt32("isActivo") == 1 ? true : false),
                                    categoria = new Categoria()
                                    {
                                        id = reader.GetInt32("idCategoria"),
                                        nombre = reader.GetString("nomCat")
                                    }
                                }
                            );
                        }
                    }

                    if (productos != null && productos.Count > 0)
                    {
                        response.Data = productos;
                        response.status.code = 0;
                        response.status.message = "succes";
                    }

                }
                catch (Exception ex)
                {
                    response.Data = null;
                    response.status.code = -2;
                    response.status.message = $"Error - {ex.Message}";
                }
                
            }         
            else
            {
                response.status.code = -2;
                response.status.message = "Error de id";
            }

            return response;
        }


        [HttpGet]
        [Route("api/Productos/getProductosTienda/{idCategoria}")]
        public ProductosGetResponse getProductosCategoria(string idCategoria)
        {
            ProductosGetResponse response = new ProductosGetResponse(new Status() { code = -1, message = "Productos no encontrados" }, null);

            if (idCategoria != null && idCategoria != "")
            {

                List<Producto> productos = new List<Producto>();

                string mycon = ConfigurationManager.AppSettings["dbconn"].ToString();
                MySqlConnection con = new MySqlConnection(mycon);


                try
                {
                    MySqlCommand cmd = new MySqlCommand(CMD_PRODUCTOS_CATEGORIA, con);
                    cmd.Parameters.AddWithValue("@idCateg", idCategoria);

                    con.Open();
                    var reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            productos.Add(
                                new Producto()
                                {
                                    id = reader.GetInt32("id"),
                                    idTienda = reader.GetInt32("idTienda"),
                                    nombre = reader.GetString("nombre"),
                                    precio = reader.GetDouble("precio"),
                                    descuento = reader.GetDouble("descuento"),
                                    inStock = (reader.GetInt32("inStock") == 1 ? true : false),
                                    cantidad = reader.GetInt32("cantidad"),
                                    isActivo = (reader.GetInt32("isActivo") == 1 ? true : false),
                                    categoria = new Categoria()
                                    {
                                        id = reader.GetInt32("idCategoria"),
                                        nombre = reader.GetString("nomCat")
                                    }
                                }
                            );
                        }
                    }

                    if (productos != null && productos.Count > 0)
                    {
                        response.Data = productos;
                        response.status.code = 0;
                        response.status.message = "succes";
                    }

                }
                catch (Exception ex)
                {
                    response.Data = null;
                    response.status.code = -2;
                    response.status.message = $"Error - {ex.Message}";
                }

            }
            else
            {
                response.status.code = -2;
                response.status.message = "Error de id";
            }

            return response;
        }


        [HttpGet]
        [Route("api/Productos/buscarProductosNombre/{nom}")]
        public ProductosGetResponse buscarProductosNombre(string nom)
        {
            ProductosGetResponse response = new ProductosGetResponse(new Status() { code = -1, message = "Productos no encontrados" }, null);

            

            if (nom != null && nom != "")
            {
                string command = CMD_BUSCAR_PRODUCTOS_NOMBRE_P_T_C.Replace("@nom", nom);

                List<Producto> productos = new List<Producto>();

                string mycon = ConfigurationManager.AppSettings["dbconn"].ToString();
                MySqlConnection con = new MySqlConnection(mycon);


                try
                {
                    MySqlCommand cmd = new MySqlCommand(command, con);
                    //cmd.Parameters.AddWithValue("@nom", nom);

                    con.Open();
                    var reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            productos.Add(
                                new Producto()
                                {
                                    id = reader.GetInt32("id"),
                                    idTienda = reader.GetInt32("idTienda"),
                                    nombre = reader.GetString("nombre"),
                                    precio = reader.GetDouble("precio"),
                                    descuento = reader.GetDouble("descuento"),
                                    inStock = (reader.GetInt32("inStock") == 1 ? true : false),
                                    cantidad = reader.GetInt32("cantidad"),
                                    isActivo = (reader.GetInt32("isActivo") == 1 ? true : false),
                                    categoria = new Categoria()
                                    {
                                        id = reader.GetInt32("idCategoria"),
                                        nombre = reader.GetString("nomCat")
                                    }
                                }
                            );
                        }
                    }

                    if (productos != null && productos.Count > 0)
                    {
                        response.Data = productos;
                        response.status.code = 0;
                        response.status.message = "succes";
                    }

                }
                catch (Exception ex)
                {
                    response.Data = null;
                    response.status.code = -2;
                    response.status.message = $"Error - {ex.Message}";
                }

            }
            else
            {
                response.status.code = -2;
                response.status.message = "Error de id";
            }

            return response;
        }


        [HttpPost]
        [Route("api/Productos/insertar")]
        public Status insertarProducto(Producto producto)
        {
            Status response = new Status() { code = -1, message = "Error al insertar" };

            string mycon = ConfigurationManager.AppSettings["dbconn"].ToString();
            MySqlConnection con = new MySqlConnection(mycon);

            try
            {

                MySqlCommand cmd = new MySqlCommand(CMD_INSERTAR, con);                
                cmd.Parameters.AddWithValue("@idTienda", producto.idTienda);
                cmd.Parameters.AddWithValue("@idCat", producto.categoria.id);
                cmd.Parameters.AddWithValue("@nom", producto.nombre);
                cmd.Parameters.AddWithValue("@precio", producto.precio);
                cmd.Parameters.AddWithValue("@descuento", producto.descuento);
                cmd.Parameters.AddWithValue("@inStock", producto.inStock);
                cmd.Parameters.AddWithValue("@cantidad", producto.cantidad);
                cmd.Parameters.AddWithValue("@isActivo", producto.isActivo);

                con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

                response.code = 0;
                response.message = "success";

            }
            catch (Exception ex)
            {
                response.code = -2;
                response.message = $"Error -- {ex.Message}";
            }

            return response;
        }

        [HttpPost]
        [Route("api/Productos/actualizar")]
        public Status actualizarProducto(Producto producto)
        {
            Status response = new Status() { code = -1, message = "Error al actualizar" };

            string mycon = ConfigurationManager.AppSettings["dbconn"].ToString();
            MySqlConnection con = new MySqlConnection(mycon);

            try
            {

                MySqlCommand cmd = new MySqlCommand(CMD_ACTUALIZAR, con);
                cmd.Parameters.AddWithValue("@idTienda", producto.idTienda);
                cmd.Parameters.AddWithValue("@idCat", producto.categoria.id);
                cmd.Parameters.AddWithValue("@nom", producto.nombre);
                cmd.Parameters.AddWithValue("@precio", producto.precio);
                cmd.Parameters.AddWithValue("@descuento", producto.descuento);
                cmd.Parameters.AddWithValue("@inStock", producto.inStock);
                cmd.Parameters.AddWithValue("@cantidad", producto.cantidad);
                cmd.Parameters.AddWithValue("@isActivo", producto.isActivo);
                cmd.Parameters.AddWithValue("@idProducto", producto.id);

                con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

                response.code = 0;
                response.message = "success";

            }
            catch (Exception ex)
            {
                response.code = -2;
                response.message = $"Error -- {ex.Message}";
            }

            return response;
        }

    }
}
