﻿using intraApi.Models;
using intraApi.Models.DTOS;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace intraApi.Controllers
{
    public class TiendasController : ApiController
    {
        private string CMD_GETALLACTIVE = "select * from tiendas where isActivo = 1";

        [HttpGet]
        [Route("api/Tiendas/getActiveTiendas")]
        public TiendasGetResponse getTiendas()
        {
            TiendasGetResponse response = new TiendasGetResponse(new Status() { code = -1, message = "Tiendas no encontradas" }, null);

            List<Tienda> tiendas = new List<Tienda>();

            string mycon = ConfigurationManager.AppSettings["dbconn"].ToString();
            MySqlConnection con = new MySqlConnection(mycon);


            try
            {
                MySqlCommand cmd = new MySqlCommand(CMD_GETALLACTIVE, con);
                
                con.Open();
                var reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        tiendas.Add(new Tienda(reader.GetInt32(0), reader.GetInt32(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5), reader.GetBoolean(6)));
                    }
                }

                if (tiendas != null && tiendas.Count > 0)
                {
                    response.Data = tiendas;
                    response.status.code = 0;
                    response.status.message = "succes";
                }                

            }
            catch (Exception ex)
            {
                response.Data = null;
                response.status.code = -2;                
                response.status.message = $"Error - {ex.Message}";
            }


            return response;

            
        }

    }
}
