﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intraApi.Models
{
    public class Productos_Pedido
    {
        public int id { set; get; }
        public int idPedido { set; get; }
        public int idProducto { set; get; }
        public int cantidad { set; get; }

        public Productos_Pedido(int id, int idPedido, int idProducto, int cantidad)
        {
            this.id = id;
            this.idPedido = idPedido;
            this.idProducto = idProducto;
            this.cantidad = cantidad;
        }
    }
}