﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intraApi.Models
{
    public class Productos_Carrito
    {
        public int id { set; get; }
        public int idCarrito { set; get; }
        public int idProducto { set; get; }
        public int cantidad { set; get; }

        public Productos_Carrito(int id, int idCarrito, int idProducto, int cantidad)
        {
            this.id = id;
            this.idCarrito = idCarrito;
            this.idProducto = idProducto;
            this.cantidad = cantidad;
        }
    }
}