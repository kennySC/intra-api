﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intraApi.Models
{
    public class Categoria
    {
        public int id { set; get; }
        public string nombre { set; get; }

        public Categoria(int id, string nombre)
        {
            this.id = id;
            this.nombre = nombre;
        }

        public Categoria()
        {
        }
    }
}