﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intraApi.Models.DTOS
{
    public class ProductosGetResponse
    {
        public Status status { set; get; }

        public List<Producto> Data { set; get; }

        public ProductosGetResponse(Status status, List<Producto> data)
        {
            this.status = status;
            Data = data;
        }
    }
}