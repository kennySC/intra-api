﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intraApi.Models.DTOS
{
    public class CarritoProductosGetResponse
    {
        public Status status { set; get; }

        public Carrito Data { set; get; }

        public CarritoProductosGetResponse()
        {
        }

        public CarritoProductosGetResponse(Status status, Carrito carrito)
        {
            this.status = status;
            this.Data = carrito;
        }
    }
}