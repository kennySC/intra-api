﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intraApi.Models.DTOS
{
    public class PedidoGetResponse
    {
        public Status status { set; get; }
        public List<Pedido> Data { set; get; }

        public PedidoGetResponse(Status status, List<Pedido> data)
        {
            this.status = status;
            Data = data;
        }
    }
}