﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intraApi.Models.DTOS
{
    public class CategoriasGetReponse
    {
        public Status status { set; get; }

        public List<Categoria> Data { set; get; }

        public CategoriasGetReponse (Status status, List<Categoria> Data)
        {
            this.status = status;
            this.Data = Data;
        }
    }
}