﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intraApi.Models.DTOS
{
    public class TiendasGetResponse
    {
        public Status status { set; get; }

        public List<Tienda> Data { set; get; }

        public TiendasGetResponse(Status status, List<Tienda> data)
        {
            this.status = status;
            Data = data;
        }
    }
}