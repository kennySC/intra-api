﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intraApi.Models.DTOS
{
    public class UsuarioLoginResponse
    {
        public Status status { set; get; }
        public Usuario Data { set; get; }

        public UsuarioLoginResponse(Status status, Usuario data)
        {
            this.status = status;
            Data = data;
        }
    }
}