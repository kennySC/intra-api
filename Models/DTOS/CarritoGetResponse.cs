﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intraApi.Models.DTOS
{
    public class CarritoGetResponse
    {
        public Status status { set; get; }

        public List<Carrito> Data { set; get; }

        public CarritoGetResponse(Status status, List<Carrito> Data)
        {
            this.status = status;
            this.Data = Data;
        }
    }
}
