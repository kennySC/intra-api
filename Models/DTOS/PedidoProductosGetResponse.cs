﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intraApi.Models.DTOS
{
    public class PedidoProductosGetResponse
    {
        public Status status { set; get; }

        public Pedido Data { set; get; }

        public PedidoProductosGetResponse(Status status, Pedido data)
        {
            this.status = status;
            Data = data;
        }

        public PedidoProductosGetResponse()
        {
        }
    }
}