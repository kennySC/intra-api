﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intraApi.Models.DTOS
{
    public class Status
    {
        public int code { set; get; }
        public string message { set; get; }
    }
}