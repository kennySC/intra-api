﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intraApi.Models
{
    public class Pedido
    {
        public int id { set; get; }
        public int idCliente { set; get; }
        public int idTienda { set; get; }
        public int idMetodoPago { set; get; }
        public double total { set; get; }
        public string codigo { set; get; }
        public DateTime fechaPedido { set; get; }
        public int isActivo { set; get; }

        public List<Producto> productosPedido { set; get; }

        public Pedido(int id, int idCliente, int idTienda, int idMetodoPago, double total, string codigo, DateTime fechaPedido, int isActivo)
        {
            this.id = id;
            this.idCliente = idCliente;
            this.idTienda = idTienda;
            this.idMetodoPago = idMetodoPago;
            this.total = total;
            this.codigo = codigo;
            this.fechaPedido = fechaPedido;
            this.isActivo = isActivo;
        }

        public Pedido()
        {
        }
    }
}