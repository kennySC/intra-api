﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intraApi.Models
{
    public class Producto
    {
        public int id { set; get; }
        public int idTienda { set; get; }
        public Categoria categoria { set; get; }
        public string nombre { set; get; }
        public double precio { set; get; }
        public double descuento { set; get; }
        public bool inStock { set; get; }
        public int cantidad { set; get; }
        public bool isActivo { set; get; }

    }
}