﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intraApi.Models
{
    public class Tienda
    {
        public Tienda(int id, int idAministrador, string nombre, string telefono, string email, string ubicacion, bool isActivo)
        {
            this.id = id;
            this.idAministrador = idAministrador;
            this.nombre = nombre;
            this.telefono = telefono;
            this.email = email;
            this.ubicacion = ubicacion;
            this.isActivo = isActivo;
        }

        public int id { set; get; }		
		public int idAministrador { set; get; }
		public string nombre { set; get; }
		public string telefono { set; get; }
		public string email { set; get; }
		public string ubicacion { set; get; }
		public bool isActivo { set; get; }

	}
}