﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intraApi.Models
{
	public class Carrito
	{
		public int id { set; get; }
		public int idCliente { set; get; }
		public int idTienda { set; get; }
		public int isActivo { set; get; }

		public List<Producto> productosCarrito { set; get; }

		public Carrito(int id, int idCliente, int idTienda, int isActivo)
		{
			this.id = id;
			this.idCliente = idCliente;
			this.idTienda = idTienda;
			this.isActivo = isActivo;
		}
		public Carrito()
		{
		}
	}
}