﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intraApi.Models
{
    public class Usuario
    {
        public Usuario(int id, string email, string nombre, string password, string telefono, bool isActivo)
        {
            this.id = id;
            this.email = email;
            this.nombre = nombre;
            this.password = password;
            this.telefono = telefono;
            this.isActivo = isActivo;
        }

        public int id { set; get; }

        public string email { set; get; }

        public string nombre { set; get; }

        public string password { set; get; }

        public string telefono { set; get; }

        public bool isActivo { set; get; }

        public string ToString()
        {
            return $" -- Usuario {id}, {nombre} -- ";
        }

    }
}